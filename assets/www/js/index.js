/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

$(document).ready(function(){
    // Search for wind on click
    $('.button').click(function(){
        $(this).fadeOut(250);
        function success(position) {

            var lat = position.coords.latitude;
            var lng = position.coords.longitude;

            $.getJSON('https://api.metwit.com/v2/weather/?location_lat='+lat+'&location_lng='+lng , function(data) {
                var wind_speed = data.objects[0].weather['measured'].wind_speed;
                var wind_direction = data.objects[0].weather['measured'].wind_direction * 2.23694;

                //Hide Start and Show Result
                $('.start').fadeOut(500,function(){
                    $('.result').fadeIn(500);
                })
                $("#weather").html('<h2 class="wind_speed">'+Math.round(wind_speed)+' <span>mph</span></h2>');
                $('.wind_direction img').css('transform', 'rotate('+wind_direction+'deg)');
            });
        }
         
        function error(msg) {
            // select the span with id status
            var s = $('#status');
            
            // set the error message
            s.html(typeof msg == 'string' ? msg : "failed");
        }

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(success, error);
        } else {
            error('not supported');
        }
    })
    // Refresh to start screen
    $('.refresh').click(function(){
        $('.result').fadeOut(500,function(){
            $('.button').show();
            $('.start').fadeIn(500);
        })
    })

});